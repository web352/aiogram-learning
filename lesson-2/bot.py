import asyncio
import os
import logging

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.utils.emoji import emojize
from aiogram.utils.markdown import text, bold, italic, code, pre
from aiogram.types.message import ContentType
from aiogram.types import ParseMode, InputMediaPhoto, InputMediaVideo, ChatActions

logging.basicConfig(format=u'%(filename)s [ LINE:%(lineno)+3s ]#%(levelname)+8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)

TOKEN = os.environ["TG_TOKEN"]

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

VOICE = 'AwACAgIAAxkDAAIBKGI_Va_LBATLDtBexarApTIQt3D1AAJ-FgACtMIAAUr2rI0C-L4t2CME'
PYTHON = 'AgACAgIAAxkDAAIBJWI_Va5dGHrHj0Wd4CpQPEIXXMTyAAKKujEbtMIAAUqox_LrNLRPHwEAAwIAA3gAAyME'
VIDEO = 'BAACAgIAAxkDAAIBKmI_VbD46eCFs6jcJMkhzcXwCWxIAAJ_FgACtMIAAUp1j37HDb3IfSME'
PHOTOS = {
    'velo': 'AgACAgIAAxkDAAIBK2I_VbGx36eA4nrloLpTWAqJhJFUAAKMujEbtMIAAUq7Cm010STsKQEAAwIAA3gAAyME',  # velo
    'argo': 'AgACAgIAAxkDAAIBLGI_VbF5k9le5p2wCYJPLx7XhhKBAAKNujEbtMIAAUqmBhJ5yZUQbAEAAwIAA20AAyME',  # argo
}
VIDEO_NOTE = 'DQACAgIAAxkDAAIBJmI_Va6yaXYv1JiwtOR22_D1ApSmAAJ8FgACtMIAAUqqPKsPAAG37nQjBA'
TEXT_FILE = 'BQACAgIAAxkDAAIBJGI_Va3PFyTEUJtSQS7fILiv0aZ0AAJ7FgACtMIAAUqs6MhPjvfgMyME'

WHITE_HOUSE = (55.754970, 37.573146)
COCOS_TATOO_CHEATSHEET = 'https://ah-public-pictures.hb.bizmrg.com/coconutStudio/coconut-tatoo-sheet.pdf'


@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.reply('Привет!\nИспользуй /help, '
                        'чтобы узнать список доступных команд!')


@dp.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    msg = text(bold('Я могу ответить на следующие команды:'),
               '/voice', '/photo', '/group', '/note', '/file', '/testpre', '/media', sep='\n')
    await message.reply(msg, parse_mode=ParseMode.MARKDOWN)


@dp.message_handler(commands=['voice'])
async def process_voice_command(message: types.Message):
    await bot.send_voice(message.from_user.id, VOICE,
                         reply_to_message_id=message.message_id)


@dp.message_handler(commands=['photo'])
async def process_photo_command(message: types.Message):
    caption = 'python! :snake:'
    await bot.send_photo(message.from_user.id, PYTHON,
                         caption=emojize(caption),
                         reply_to_message_id=message.message_id)


@dp.message_handler(commands=['group'])
async def process_group_command(message: types.Message):
    media = [InputMediaVideo(VIDEO, 'Andrew и картинки')]
    for photo_id in PHOTOS:
        media.append(InputMediaPhoto(PHOTOS[photo_id], photo_id))
    await bot.send_media_group(message.from_user.id, media)


@dp.message_handler(commands=['note'])
async def process_note_command(message: types.Message):
    user_id = message.from_user.id
    await bot.send_chat_action(user_id, ChatActions.RECORD_VIDEO_NOTE)
    await asyncio.sleep(1)  # конвертируем видео и отправляем его пользователю
    await bot.send_video_note(message.from_user.id, VIDEO_NOTE)


@dp.message_handler(commands=['file'])
async def process_file_command(message: types.Message):
    user_id = message.from_user.id
    await bot.send_chat_action(user_id, ChatActions.UPLOAD_DOCUMENT)
    await asyncio.sleep(1)  # скачиваем файл и отправляем его пользователю
    await bot.send_document(user_id, TEXT_FILE,
                            caption='Этот файл специально для тебя!')

    await asyncio.sleep(1)
    await types.ChatActions.upload_document()
    await bot.send_document(message.from_user.id, COCOS_TATOO_CHEATSHEET)


@dp.message_handler(commands=['testpre'])
async def process_testpre_command(message: types.Message):
    message_text = pre(emojize('''@dp.message_handler(commands=['testpre'])
async def process_testpre_command(message: types.Message):
    message_text = pre(emojize('Ха! Не в этот раз :smirk:'))
    await bot.send_message(message.from_user.id, message_text)'''))
    await bot.send_message(message.from_user.id, message_text,
                           parse_mode=ParseMode.MARKDOWN)


@dp.message_handler(commands=['media'])
async def process_media_command(message: types.Message):
    await message.reply("Do you want to see many pussies? Are you ready?")
    await asyncio.sleep(2)
    await types.ChatActions.upload_photo()
    # await message.reply("Это все!")

    # https://ah-public-pictures.hb.bizmrg.com/it-happens/andrew-batman.mp4
    # https://ah-public-pictures.hb.bizmrg.com/it-happens/notion-video.pdf

    media = types.MediaGroup()

    for k in PHOTOS:
        media.attach_photo(PHOTOS[k], k)

    # media.attach_document('https://ah-public-pictures.hb.bizmrg.com/it-happens/notion-video.pdf', 'My notion Example!')
    # media.attach_video('https://ah-public-pictures.hb.bizmrg.com/it-happens/andrew-batman.mp4', 'Я бэтмэн')
    media.attach_video(VIDEO, 'Я бэтмэн')

    await message.reply_media_group(media=media)


@dp.message_handler(commands=['location'])
async def process_location_command(message: types.Message):
    await message.reply("Как пройти к Белому Дому?")
    await asyncio.sleep(1)
    await types.ChatActions.find_location()

    message_text = text(
        emojize('Метро :train2: : '), bold('Краснопресненская\n'),
        'Далее просто идем по Конюшковской улице до', emojize('"Белого дома" :house:'),
        sep=""
    )

    await bot.send_message(message.from_user.id, message_text,
                           parse_mode=ParseMode.MARKDOWN)

    lat, long = WHITE_HOUSE
    await message.reply_location(lat, long)


@dp.message_handler()
async def echo_message(msg: types.Message):
    await bot.send_message(msg.from_user.id, msg.text)


@dp.message_handler(content_types=ContentType.ANY)
async def unknown_message(msg: types.Message):
    message_text = text(emojize('Я не знаю, что с этим делать :astonished:'),
                        italic('\nЯ просто напомню,'), 'что есть',
                        code('команда'), '/help')
    await msg.reply(message_text, parse_mode=ParseMode.MARKDOWN)


if __name__ == '__main__':
    executor.start_polling(dp)
